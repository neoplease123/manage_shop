CREATE DATABASE MANAGE_SHOP
USE MANAGE_SHOP
-----------------------------------------CREATE TABLE----------------------------------------------------------------------------
-- Table mst_categories (ProductCode  , ProductName , JanCode, VendorCode, Unit , ExipireType , Image)
CREATE TABLE Mst_Categories
(
	ProductCode varchar(20) Primary Key ,
	ProductName varchar(255) NOT NULL ,
	JanCode varchar(20) NOT NULL ,
	VendorCode varchar(10)  NOT NULL,
	Unit varchar(10) NOT NULL , 
	ExipireType int , -- 1 
	Image varchar(255) NOT NULL , 
);
-- Table Mst_location(LocationCode, Image)
CREATE TABLE Mst_location
(
	LocationCode varchar(20) Primary Key ,
	Image varchar(255) NOT NULL , 
);
-- Table Mst_expend (ExpendCode , Reason , Spend , Expend_dt , Status , Note)
CREATE TABLE Mst_expend 
(
	ExpendCode int  Primary Key, -- 1 
	Reason varchar(255) NOT NULL , 
	Spend Decimal NOT NULL , 
	Expend_dt DateTime NOT NULL , 
	Status int  , --  0 : co hang , 1 : da ban , 2: vut bo , 3 : that lac 
	Note varchar(255) ,
);
-- Table Dat_Product (Product_SEQ , ProductCode , ExpireDate , LocationCode , Cost , Price , Status , Ins_dt , Upd_dt , Note)
CREATE TABLE Dat_Product 
(
	Product_SEQ varchar(255) Primary Key ,
	ProductCode varchar(20) NOT NULL ,
	ExpireDate Date NOT NULL ,
	LocationCode varchar(20) NOT  NULL,
	Cost Decimal NOT NULL , 
	Price Decimal NOT NULL ,
	Status int NOT NULL ,
	Ins_dt DATETIME Not Null ,
	Upd_dt DATETIME NOT NULL ,
	Note varchar(255) ,
);
-- Table Dat_Bill (BillCode , Bill_dt , Status , Total , Note)
CREATE TABLE Dat_Bill 
(
	BillCode varchar(20)  Primary Key, 
	Bill_dt  DATETIME , 
	Status int , -- 0: sell , 1: Return 
	Total Decimal , 
	Note varchar(255) , 
);
-- Table Dat_BillDetails (BillCode , Product_SEQ , Price)
CREATE TABLE Dat_BillDetails
(
	BillCode varchar(20) Primary Key ,
	Product_SEQ varchar(255) NOT NULL , 
	Price Decimal NOT NULL ,
);



-----------------------------------------VALUES TABLE----------------------------------------------------------------------------
-- VALUES TABLE mst_categories (ProductCode  , ProductName , JanCode, VendorCode, Unit , ExipireType , Image)
INSERT INTO Mst_Categories VAlUES ("", "", "" , "" , "" ,"" ,  , "" );
INSERT INTO Mst_Categories VAlUES ("", "", "" , "" , "" ,"" ,  , "" );
INSERT INTO Mst_Categories VAlUES ("", "", "" , "" , "" ,"" ,  , "" );
-- VALUES TABLE Mst_location(LocationCode, Image)
INSERT INTO Mst_location VALUES ("" , "");
INSERT INTO Mst_location VALUES ("" , "");
INSERT INTO Mst_location VALUES ("" , "");
-- VALUES Table Mst_expend (ExpendCode , Reason , Spend , Expend_dt , Status , Note)
INSERT INTO Mst_expend VALUES ( , "" , , " - - -" ,  , "");
INSERT INTO Mst_expend VALUES ( , "" , , " - - -" ,  , "");
INSERT INTO Mst_expend VALUES ( , "" , , " - - -" ,  , "");
-- VALUES Table Dat_Product (Product_SEQ , ProductCode , ExpireDate , LocationCode , Cost , Price , Status , Ins_dt , Upd_dt , Note)
INSERT INTO Dat_Bill VALUES ( , "" ," - - - ", "" , , , , "- - -" , "- - -" , " ");
INSERT INTO Dat_Bill VALUES ( , "" ," - - - ", "" , , , , "- - -" , "- - -" , " ");
INSERT INTO Dat_Bill VALUES ( , "" ," - - - ", "" , , , , "- - -" , "- - -" , " ");
-- VALUES Table Dat_Bill (BillCode , Bill_dt , Status , Total , Note)
INSERT INTO Dat_Bill VALUES (" " ," - - - " , , , "");
INSERT INTO Dat_Bill VALUES (" " ," - - - " , , , "");
INSERT INTO Dat_Bill VALUES (" " ," - - - " , , , "");
-- VALUES Table Dat_BillDetails (BillCode , Product_SEQ , Price)
INSERT INTO Dat_BillDetails VALUES ("" , "" , );
INSERT INTO Dat_BillDetails VALUES ("" , "" , );
INSERT INTO Dat_BillDetails VALUES ("" , "" , );