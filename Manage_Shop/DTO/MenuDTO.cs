﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manage_Shop.DTO
{
    public class MenuDTO
    {
        public MenuDTO(string productname , int count , int price , int totalprice = 0)
        {
            this.ProductName = productname;
            this.Count = count;
            this.Price = price;
            this.TotalPrice = totalprice;
        }


        public MenuDTO(DataRow row)
        {
            this.ProductName = row["productcode"].ToString();
            this.Count = (int)row["count"];
            this.Price = (int)row["price"];
            this.TotalPrice = (int)row["totalprice"];
        }

        public int TotalPrice { get; set; }
        public int Price { get; set; }
        public int Count { get; set; }
        public string ProductName { get; set; }
    }
}
