﻿using Manage_Shop.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manage_Shop.DAO
{
    class CategoryDAO
    {
        public static CategoryDAO instance
        {
            get { if (instance == null) instance = new CategoryDAO(); return CategoryDAO.instance; }
            private set { CategoryDAO.instance = value; }
        }

        private CategoryDAO() { }
        
        public List<Category> GetCategories()
        {
            List<Category> list = new List<Category>();

            string query = "SELECT * FROM Mst_Categories ";

            DataTable data = Fucstions.DataProvider.Instance.ExecuteQuery(query);

            foreach (DataRow item in data.Rows) 
            {
                Category category = new Category(item);
                list.Add(category);
            }

            return list;
        }
    }
}
