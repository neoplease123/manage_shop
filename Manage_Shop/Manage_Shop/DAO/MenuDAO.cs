﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manage_Shop.DTO;
namespace Manage_Shop.DAO
{
    public class MenuDAO
    {

        public static MenuDAO instance
        {
            get { if (instance == null) instance = new MenuDAO(); return MenuDAO.instance; }
            private set { MenuDAO.instance = value; }
        }

        private MenuDAO() { }
        public List<MenuDTO> GetListMenuByTable()
        {
            List<MenuDTO> listMenu = new List<MenuDTO>();

            string query = "SELECT ca.ProductName,pd.Price, pd.Count , pd.Price*pd.Count AS TotalPrice  FROM Mst_Categories AS ca, Dat_Product AS pd WHERE ca.ProductCode = pd.ProductCode";

            DataTable data = Fucstions.DataProvider.Instance.ExecuteQuery(query);

            foreach (DataRow item in data.Rows)
            {
                MenuDTO menu = new MenuDTO(item);
                listMenu.Add(menu);
            }
            return listMenu;
        }
    }
}
