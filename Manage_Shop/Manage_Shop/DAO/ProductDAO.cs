﻿using Manage_Shop.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Manage_Shop.DAO
{
    class ProductDAO
    {
        public static ProductDAO instance
        {
            get { if (instance == null) instance = new ProductDAO(); return ProductDAO.instance; }
            private set { ProductDAO.instance = value; }
        }

        public ProductDAO() { }

        List<Product> GetProductsByID(string id)
        {
            List<Product> list = new List<Product>();
            string query = string.Format("SELECT * FROM Product WHERE ProductCode = '{0}' " , id);
            DataTable data = Fucstions.DataProvider.Instance.ExecuteQuery(query);

            foreach (DataRow item in data.Rows)
            {
                Product product = new Product(item);
                list.Add(product);
            }
            return list;
        }
    }
}
