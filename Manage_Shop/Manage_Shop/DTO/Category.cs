﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manage_Shop.DTO
{
    class Category
    {

        public Category(string id , string name , string jancode, string vencode , string unit , int exipiretype , string image)
        {
            this.ID = id;
            this.Name = name;
            this.JanCode = jancode;
            this.Vencode = vencode;
            this.Unit = unit;
            this.ExipireType = exipiretype;
            this.Image = image;
        }

        public Category(DataRow row)
        {
            this.ID = row["id"].ToString();
            this.Name = row["name"].ToString();
            this.JanCode = row["jancode"].ToString();
            this.Vencode = row["vencode"].ToString();
            this.Unit = row["unit"].ToString();
            this.ExipireType = (int)row["exipiretype"];
            this.Image = row["image"].ToString();
        }

        public string Image { set; get; }
        public int ExipireType { set; get; }
        public string Unit { set; get; }
        public string Vencode { set; get; }
        public string JanCode { set; get; }
        public string Name { set; get; }
        public string ID { set; get; }

    }
}
