﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace Manage_Shop.DTO
{
    // productSEQ, productCode , ExpireDate, LocationCode,int-Cost ,int-Price , int-Status , ins_dt , Update_dt , Note
    class Product
    {
        public Product(string productSEQ, string productCode, string expireDate, int cost, int price, string insertDate, string updateDate, string note)
        {
            this.ProductSEQ = productSEQ;
            this.ProductCode = productCode;
            this.ExpireDate = expireDate;
            this.Cost = cost;
            this.Price = price;
            this.InsertDate = insertDate;
            this.UpdateDate = updateDate;
            this.Note = note;
        }

        public Product(DataRow row)
        {
            this.ProductSEQ = row["productSEQ"].ToString();
            this.ProductCode = row["productCode"].ToString();
            this.ExpireDate = row["expireDate"].ToString();
            this.Cost = (int)row["cost"];
            this.Price = (int)row["price"];
            this.InsertDate = row["insertDate"].ToString();
            this.UpdateDate = row["updateDate"].ToString();
            this.Note = row["note"].ToString();
        }

        public string ProductSEQ { set; get; }
        public string ProductCode { set; get; }
        public string ExpireDate { set; get; }
        public string LocationCode { set; get; }
        public int Cost { set; get; }
        public int Price { set; get; }
        public string InsertDate { set; get; }
        public string UpdateDate { set; get; }
        public string Note { set; get; }


    }
}
