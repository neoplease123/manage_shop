﻿
namespace Manage_Shop
{
    partial class ManHinhChinh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.BtnThongKe = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btnChiPhi = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnThanhToan = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnQLCH = new System.Windows.Forms.Button();
            this.BtnThoat = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Controls.Add(this.BtnThongKe);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.btnChiPhi);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.btnThanhToan);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.btnQLCH);
            this.panel1.Controls.Add(this.BtnThoat);
            this.panel1.Location = new System.Drawing.Point(12, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1042, 456);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Manage_Shop.Properties.Resources.Thong_KE;
            this.pictureBox4.Location = new System.Drawing.Point(56, 243);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(207, 140);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 12;
            this.pictureBox4.TabStop = false;
            // 
            // BtnThongKe
            // 
            this.BtnThongKe.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnThongKe.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.BtnThongKe.Location = new System.Drawing.Point(56, 389);
            this.BtnThongKe.Name = "BtnThongKe";
            this.BtnThongKe.Size = new System.Drawing.Size(207, 47);
            this.BtnThongKe.TabIndex = 11;
            this.BtnThongKe.Text = "Thống Kê";
            this.BtnThongKe.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BtnThongKe.UseVisualStyleBackColor = true;
            this.BtnThongKe.Click += new System.EventHandler(this.BtnThongKe_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Manage_Shop.Properties.Resources.Chi_Phi;
            this.pictureBox3.Location = new System.Drawing.Point(780, 18);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(207, 140);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 10;
            this.pictureBox3.TabStop = false;
            // 
            // btnChiPhi
            // 
            this.btnChiPhi.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChiPhi.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnChiPhi.Location = new System.Drawing.Point(780, 164);
            this.btnChiPhi.Name = "btnChiPhi";
            this.btnChiPhi.Size = new System.Drawing.Size(207, 47);
            this.btnChiPhi.TabIndex = 9;
            this.btnChiPhi.Text = "Chi Phí";
            this.btnChiPhi.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnChiPhi.UseVisualStyleBackColor = true;
            this.btnChiPhi.Click += new System.EventHandler(this.btnChiPhi_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.pictureBox2.Image = global::Manage_Shop.Properties.Resources.thanhtoan;
            this.pictureBox2.Location = new System.Drawing.Point(406, 18);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(207, 140);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            // 
            // btnThanhToan
            // 
            this.btnThanhToan.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThanhToan.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnThanhToan.Location = new System.Drawing.Point(406, 164);
            this.btnThanhToan.Name = "btnThanhToan";
            this.btnThanhToan.Size = new System.Drawing.Size(207, 47);
            this.btnThanhToan.TabIndex = 7;
            this.btnThanhToan.Text = "Thanh Toán";
            this.btnThanhToan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnThanhToan.UseVisualStyleBackColor = true;
            this.btnThanhToan.Click += new System.EventHandler(this.btnThanhToan_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pictureBox1.Image = global::Manage_Shop.Properties.Resources.QuanLyCuaHang;
            this.pictureBox1.Location = new System.Drawing.Point(56, 18);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(207, 140);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // btnQLCH
            // 
            this.btnQLCH.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQLCH.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnQLCH.Location = new System.Drawing.Point(56, 164);
            this.btnQLCH.Name = "btnQLCH";
            this.btnQLCH.Size = new System.Drawing.Size(207, 47);
            this.btnQLCH.TabIndex = 5;
            this.btnQLCH.Text = "Quản Lý Cửa Hàng";
            this.btnQLCH.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnQLCH.UseVisualStyleBackColor = true;
            this.btnQLCH.Click += new System.EventHandler(this.btnQLCH_Click);
            // 
            // BtnThoat
            // 
            this.BtnThoat.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnThoat.Location = new System.Drawing.Point(757, 370);
            this.BtnThoat.Name = "BtnThoat";
            this.BtnThoat.Size = new System.Drawing.Size(264, 72);
            this.BtnThoat.TabIndex = 4;
            this.BtnThoat.Text = "Thoát";
            this.BtnThoat.UseVisualStyleBackColor = true;
            this.BtnThoat.Click += new System.EventHandler(this.BtnThoat_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1066, 483);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button BtnThoat;
        private System.Windows.Forms.Button btnQLCH;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button BtnThongKe;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button btnChiPhi;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnThanhToan;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

