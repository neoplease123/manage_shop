﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Manage_Shop
{
    public partial class ManHinhChinh : Form
    {
        public ManHinhChinh()
        {
            InitializeComponent();
        }

        private void btnQLCH_Click(object sender, EventArgs e)
        {
            f_Manage.QuanLyCuaHang qlch = new f_Manage.QuanLyCuaHang();
            qlch.ShowDialog();
        }

        private void btnThanhToan_Click(object sender, EventArgs e)
        {
            f_Manage.ThanhToan thanhtoan = new f_Manage.ThanhToan();
            thanhtoan.ShowDialog();
        }

        private void btnChiPhi_Click(object sender, EventArgs e)
        {
            f_Manage.ChiPhi chiphi = new f_Manage.ChiPhi();
            chiphi.ShowDialog();
        }

        private void BtnThongKe_Click(object sender, EventArgs e)
        {
            f_Manage.ThongKe thongke = new f_Manage.ThongKe();
            thongke.ShowDialog();
        }

        private void BtnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
