﻿
namespace Manage_Shop.f_Manage
{
    partial class QuanLyCuaHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_DELETE = new System.Windows.Forms.Button();
            this.btn_Quay = new System.Windows.Forms.Button();
            this.btn_Insert = new System.Windows.Forms.Button();
            this.btn_Update = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_Find = new System.Windows.Forms.Button();
            this.cbb_Find = new System.Windows.Forms.ComboBox();
            this.txt_Find = new System.Windows.Forms.TextBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(976, 519);
            this.panel1.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.dataGridView2);
            this.panel5.Location = new System.Drawing.Point(557, 44);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(412, 420);
            this.panel5.TabIndex = 3;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btn_DELETE);
            this.panel4.Controls.Add(this.btn_Quay);
            this.panel4.Controls.Add(this.btn_Insert);
            this.panel4.Controls.Add(this.btn_Update);
            this.panel4.Location = new System.Drawing.Point(6, 477);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(966, 35);
            this.panel4.TabIndex = 2;
            // 
            // btn_DELETE
            // 
            this.btn_DELETE.Location = new System.Drawing.Point(311, 4);
            this.btn_DELETE.Name = "btn_DELETE";
            this.btn_DELETE.Size = new System.Drawing.Size(143, 31);
            this.btn_DELETE.TabIndex = 2;
            this.btn_DELETE.Text = "Xóa";
            this.btn_DELETE.UseVisualStyleBackColor = true;
            // 
            // btn_Quay
            // 
            this.btn_Quay.Location = new System.Drawing.Point(820, 3);
            this.btn_Quay.Name = "btn_Quay";
            this.btn_Quay.Size = new System.Drawing.Size(143, 31);
            this.btn_Quay.TabIndex = 1;
            this.btn_Quay.Text = "Quay";
            this.btn_Quay.UseVisualStyleBackColor = true;
            // 
            // btn_Insert
            // 
            this.btn_Insert.Location = new System.Drawing.Point(162, 3);
            this.btn_Insert.Name = "btn_Insert";
            this.btn_Insert.Size = new System.Drawing.Size(143, 31);
            this.btn_Insert.TabIndex = 1;
            this.btn_Insert.Text = "Thêm ";
            this.btn_Insert.UseVisualStyleBackColor = true;
            // 
            // btn_Update
            // 
            this.btn_Update.Location = new System.Drawing.Point(13, 3);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.Size = new System.Drawing.Size(143, 31);
            this.btn_Update.TabIndex = 0;
            this.btn_Update.Text = "Chỉnh sửa";
            this.btn_Update.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dataGridView1);
            this.panel3.Location = new System.Drawing.Point(6, 44);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(545, 427);
            this.panel3.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(4, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(538, 417);
            this.dataGridView1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btn_Find);
            this.panel2.Controls.Add(this.cbb_Find);
            this.panel2.Controls.Add(this.txt_Find);
            this.panel2.Location = new System.Drawing.Point(6, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(966, 35);
            this.panel2.TabIndex = 0;
            // 
            // btn_Find
            // 
            this.btn_Find.Location = new System.Drawing.Point(668, 0);
            this.btn_Find.Name = "btn_Find";
            this.btn_Find.Size = new System.Drawing.Size(191, 35);
            this.btn_Find.TabIndex = 2;
            this.btn_Find.Text = "Tìm Kiếm";
            this.btn_Find.UseVisualStyleBackColor = true;
            // 
            // cbb_Find
            // 
            this.cbb_Find.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.cbb_Find.DropDownWidth = 121;
            this.cbb_Find.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbb_Find.ForeColor = System.Drawing.SystemColors.WindowText;
            this.cbb_Find.FormattingEnabled = true;
            this.cbb_Find.Items.AddRange(new object[] {
            "Hàng Hóa ",
            "Nhà Cung Cấp"});
            this.cbb_Find.Location = new System.Drawing.Point(526, 8);
            this.cbb_Find.Name = "cbb_Find";
            this.cbb_Find.Size = new System.Drawing.Size(136, 21);
            this.cbb_Find.TabIndex = 1;
            this.cbb_Find.Text = "Hàng Hóa";
            // 
            // txt_Find
            // 
            this.txt_Find.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.txt_Find.Location = new System.Drawing.Point(4, 8);
            this.txt_Find.Multiline = true;
            this.txt_Find.Name = "txt_Find";
            this.txt_Find.Size = new System.Drawing.Size(516, 21);
            this.txt_Find.TabIndex = 0;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(4, 4);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(405, 416);
            this.dataGridView2.TabIndex = 0;
            // 
            // QuanLyCuaHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 537);
            this.Controls.Add(this.panel1);
            this.Name = "QuanLyCuaHang";
            this.Text = "QuanLyCuaHang";
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btn_DELETE;
        private System.Windows.Forms.Button btn_Quay;
        private System.Windows.Forms.Button btn_Insert;
        private System.Windows.Forms.Button btn_Update;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_Find;
        private System.Windows.Forms.ComboBox cbb_Find;
        private System.Windows.Forms.TextBox txt_Find;
        private System.Windows.Forms.DataGridView dataGridView2;
    }
}