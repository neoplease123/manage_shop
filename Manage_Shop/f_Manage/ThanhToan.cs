﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Manage_Shop.DTO;
namespace Manage_Shop.f_Manage
{
    public partial class ThanhToan : Form
    {
        private string connectionSTR = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\ADMIN\\MANAGE_SHOP.mdf;Persist Security Info=True;User ID=sa;Password=1234;Connect Timeout=30";
        SqlCommand cmd;
        public ThanhToan()
        {
            InitializeComponent();
            LoadProduct();     
        }

        void LoadProduct()
        {
            string query = " SELECT *  FROM Mst_Categories ";
            dgvTT.DataSource = Fucstions.DataProvider.Instance.ExecuteQuery(query);
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            string name = txtFind.Text.Trim();
            string query = string.Format("SELECT * FROM Mst_Categories WHERE ProductCode = N'{0}'", name);
            using (SqlConnection conn = new SqlConnection(connectionSTR))
            {
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                da.Fill(ds, "Mst_Categories");
                if (ds.Tables["Mst_Categories"].Rows.Count > 0)
                {
                    dgvTT.DataSource = ds.Tables["Mst_Categories"];
                }
                else
                    MessageBox.Show("Khong tim thay");
                conn.Close();
            }
        }

        private void cbbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd;
            using (SqlConnection conn = new SqlConnection(connectionSTR))
            {
                cmd = new SqlCommand("SELECT * FROM Dat_Product WHERE ProductCode ='" + cbbMaSp.Text + "'", conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader dr;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string price = dr["Price"].ToString();
                    txtPrice.Text = price;
                }
                conn.Close();
            }
        }

        private void ThanhToan_Load(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connectionSTR))
            {
                conn.Open();
                cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT ProductCode From Dat_Product";
                this.cbbMaSp.DataSource = null;
                this.cbbMaSp.Items.Clear();
                cmd.ExecuteNonQuery();
                DataTable data = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(data);
                foreach (DataRow dr in data.Rows)
                {
                    cbbMaSp.Items.Add(dr["ProductCode"]).ToString();
                }
                cbbMaSp.DisplayMember = "ProductCode";
                conn.Close();
            }
            timer1.Start();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            int soluong = int.Parse(nbSLthem.Value.ToString());
            int total = Convert.ToInt32(this.txtPrice.Text) * soluong;
            total = int.Parse(total.ToString());
            // them san pham
            ListViewItem item = new ListViewItem(new string[] { cbbMaSp.Text, txtPrice.Text, soluong.ToString(), total.ToString() });
            this.lvOrder.Items.AddRange(new ListViewItem[] { item });
            cbbMaSp.Text = "";
            nbSLthem.Value = 1;
            txtPrice.Clear();
            cbbMaSp.Focus();
            TongGiaTien();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if(lvOrder.Items.Count > 0)
            {
                lvOrder.Items.Remove(lvOrder.SelectedItems[0]);
            }
        }
        void TongGiaTien()
        {
            string price;
            int totalprice = 0;
            for(int i = 0; i < lvOrder.Items.Count; i++)
            {
                price = lvOrder.Items[i].SubItems[3].Text;
                totalprice += Convert.ToInt32(price);
            }
            txtTotalPrice.Text = totalprice.ToString();
        }

        private void BtnThanhToan_Click(object sender, EventArgs e)
        {
            lvOrder.Items.Clear();
            txtTotalPrice.Text = "";
            if (MessageBox.Show("Bạn chắc Chắn Thanh Toán chứ" , "Thông báo", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                MessageBox.Show("bạn đã thanh toán thành công");
                INSERT();
            }
        }

        public void INSERT()
        {
            string name = "";
            for(int i = 0; i < lvOrder.Items.Count; i++)
            {
                name += lvOrder.Items[i].Text +",";
            }
            string code = dtpTime.Text.Trim('-')+name.Trim(',');
            string totalprice = txtTotalPrice.Text;
            int status = 0;
            string datetime = lbtime.Text + " " + dtpTime.Text;
            string date = dtpTime.Text;
            string query = string.Format("INSERT INTO Dat_Bill( BillCode ,Status , Total , Note , bill_dat , Bill_d ) VALUES ('{0}',{1},{2},'{3}','{4}','{5}')", code,status,totalprice,name,datetime,date);
            Fucstions.DataProvider.Instance.ExecuteNonQuery(query);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbtime.Text = DateTime.Now.ToString("HH:mm:ss.000");
        }
        public void SetMyCustom()
        {
            dtpTime.Format = DateTimePickerFormat.Custom;
            dtpTime.CustomFormat = "yyyy-MM-dd";
        }
    }
}
